/* Oefening 3 vervolg:
STAP 3:
Maak de view AuteursBoekenRatings aan door een nieuwe view te maken 
gebaseerd op AuteursBoeken en GemiddeldeRatings. 
Noem het script dat de view maakt 0617__Oefening.sql. */

USE ModernWays;
CREATE VIEW AuteursBoekenRatings AS
SELECT Auteur, Titel, Rating
FROM Auteursboeken
INNER JOIN GemiddeldeRatings ON Auteursboeken.Boeken_Id = GemiddeldeRatings.Boeken_Id;

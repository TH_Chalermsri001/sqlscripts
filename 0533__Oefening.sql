USE ModernWays;
	-- Schrijf een script, 0533__Oefening.sql, 
    -- waarmee alle kolommen van alle boeken gezocht worden waarvoor 'van' 
    -- voorkomt in de familienaam van de auteur. 
    -- Dit kan met een kleine letter of een hoofdletter zijn.
    
    SELECT * FROM Boeken WHERE Familienaam LIKE '%van%';
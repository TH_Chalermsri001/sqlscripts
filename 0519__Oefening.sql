USE ModernWays;
	-- de niet-verplichte kolom Genre terug toevoegt aan de tabel met nummers (tot 20 karakters uit het Engels)
	-- met maximum één UPDATE-operatie alle nummers van Led Zeppelin en Van Halen aanduidt als Hard Rock
    
    USE ModernWays;
ALTER TABLE Liedjes ADD COLUMN Genre VARCHAR(20);
SET SQL_SAFE_UPDATES = 0;
UPDATE Liedjes SET Genre = 'Hard Rock' WHERE Artiest = 'Led Zeppelin' or Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES =1;
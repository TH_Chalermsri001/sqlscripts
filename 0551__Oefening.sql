-- Primaire Sleutels
-- Oefening 3
-- Lees alle baasjes af uit de tabel met huisdieren en vul op basis hiervan de tabel Baasjes in, 
-- zodat de primaire sleutelwaarden automatisch bepaald worden door MySQL. 
-- Noem dit script 0551__Oefening.sql.

USE ModernWays;
-- SELECT Baasje FROM Huisdieren;
INSERT INTO Baasjes (
Baasje)
VALUES
('Vincent'), ('Christiane'), ('Esther'), ('Bert'), ('Lyssa');
-- Oefening 6
-- chrijf een script, 0544__Oefening.sql, 
-- dat je het totaal beluisteringen toont voor elke artiest met minstens 10 karakters in de naam.
USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Totaal aantal beluisteringen'
FROM Liedjes
WHERE Length(Artiest) >= 10
GROUP BY Artiest;
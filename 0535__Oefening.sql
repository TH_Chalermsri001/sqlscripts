USE ModernWays;
	SET SQL_SAFE_UPDATES = 0;
-- LIKE '%9%' of '__9_' of '189_' zou ook kunnen ("_underscores" is per character)
-- maar dit zou ook boeken van het jaar 1890 of eerder kunnen verwijderen
	DELETE FROM Boeken WHERE Verschijningsjaar LIKE '199_';
	SET sql_safe_updates = 1;
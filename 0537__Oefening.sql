-- Groeperen en tellen
-- Schrijf een script, 0537__Oefening.sql, dat toont hoeveel boeken elke auteur heeft geschreven. 
-- Je mag ervan uitgaan dat er geen twee auteurs zijn met dezelfde combinatie van voornaam en achternaam.
USE ModernWays;
SELECT Voornaam, Familienaam, COUNT(*) AS 'Aantal Boeken'
FROM ModernWays.Boeken
GROUP BY Voornaam, Familienaam;
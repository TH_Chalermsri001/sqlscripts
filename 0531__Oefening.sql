USE ModernWays;
	-- Toon met een script, 0531__Oefening.sql, 
    -- alle boeken zonder commentaar waarvan de auteur een achternaam heeft 
    -- die met een letter vanaf "D" begint.
    SELECT * FROM Boeken WHERE Familienaam > 'D' AND Commentaar IS NULL;
    -- IS NULL => Geeft kolommen/cellen weer zonder ingegeven data
    -- AND => selecteert beide "variabelen"
USE ModernWays;
	CREATE TABLE huisdieren(
    Naam VARCHAR(100) CHAR SET utf8mb4 not null, 
    -- "not null" = requiered field (verplicht iets in te vullen)
    Leeftijd INT(255) unsigned not null,
    -- INT = MAX 255
    -- "unsigned" "not null" = cijfer kan niet onder 0 gaan
    -- "CHAR" = vaste lengte
    -- "VARCHAR" = variabele lengte
    Soort VARCHAR(50)
    );
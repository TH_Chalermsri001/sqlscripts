-- Oefening 4
-- Schrijf een script, 0542__Oefening.sql, 
-- dat alle artiesten toont waar al meer dan 100 keer naar geluisterd is. 
USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Aantal beluisteringen'
FROM Liedjes
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) >= 100;
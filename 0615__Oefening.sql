/* Oefening 3:
We willen een complexere versie van de bestaande view AuteursBoeken. 
We zullen hierbij ook de gemiddelde rating van elk boek plaatsen in een uitgebreide versie van de view, 
namelijk AuteursBoekenRatings. 
We zullen dit in stappen doen. Lees eerst de stappen, bekijk dan de figuren, voer dan de stappen uit: */

/* STAP 1: 
Gebruik een ALTER VIEW om je bestaande view AuteursBoeken te voorzien van het Id uit de tabelBoeken. 
Toon Id hier wel als Boeken_Id. 
Noem het script dat de view aanpast 0615__Oefening.sql: */

USE ModernWays;
ALTER VIEW Auteursboeken AS
SELECT CONCAT ( Personen.Voornaam,' ',Personen.Familienaam ) AS Auteur, Titel, boeken.Id AS Boeken_Id
FROM Boeken INNER JOIN Publicaties ON Boeken.Id = Publicaties.Boeken_id
INNER JOIN Personen ON Publicaties.Personen_Id = Personen.Id;
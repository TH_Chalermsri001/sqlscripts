-- primaire sleutels
-- Oefening 1
-- Schrijf een script, 0549__Oefening.sql, 
-- dat de tabel Huisdieren voorziet van een primaire sleutel. 
-- Deze begint vanaf 1 en is van het type INT

USE ModernWays;
ALTER TABLE Huisdieren
ADD Id INT AUTO_INCREMENT PRIMARY KEY;

-- ALTER TABLE TableName
-- ADD Id INT AUTO_INCREMENT PRIMARY;
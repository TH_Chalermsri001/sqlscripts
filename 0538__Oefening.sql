-- Oefening 2
-- Schrijf een script, 0538__Oefening.sql, 
-- dat toont hoeveel liedjes van Layla Zoe er in het systeem zitten.
USE ModernWays;
SELECT Artiest, COUNT(*) AS 'Totaal aantal liedjes'
FROM Liedjes
WHERE Artiest = 'Layla Zoe';
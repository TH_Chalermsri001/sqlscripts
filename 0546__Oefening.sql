-- Korte notaties
-- Oefening 1
-- Schrijf een script, 0546__Oefening.sql, 
-- dat nagaat of het totaal aantal beluisteringen van een artiest ofwel 17, ofwel 50, ofwel 100 is.
USE ModernWays;
SELECT Artiest, SUM(AantalBeluisteringen) IN (17,50,100) AS 'Specifieke aantal beluisteringen'
FROM Liedjes
GROUP BY Artiest;
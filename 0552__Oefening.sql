-- VREEMDE SLEUTELS
-- Oefening 1
-- Voeg een kolom toe aan de tabel Baasjes die je zal gebruiken om naar de tabel Huisdieren te verwijzen. 
-- Volg hierbij de conventies rond vreemde sleutels. 
-- Voorlopig moet je een NULL-waarde toestaan. Noem dit 0552__Oefening.sql.

USE ModernWays;
ALTER TABLE Baasjes -- Tabel wijzigen
ADD COLUMN Huisdieren_Id INT, -- Kolom toevoegen
ADD CONSTRAINT fk_Huisdieren_Id -- CONSTRAINT toevoegen
FOREIGN KEY (Huisdieren_Id) -- FOREIGN KEY
REFERENCES Huisdieren(Id); -- REFERENCES
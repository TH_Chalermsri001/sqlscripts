/* Oefening 3 vervolg:
STAP 2: 
Maak een view GemiddeldeRatings aan op basis van Reviews. 
Noem de kolom met het gemiddelde Rating. 
Noem het script dat de view maakt 0616__Oefening.sql. */

USE ModernWays;
CREATE VIEW GemiddeldeRatings AS
SELECT Reviews.Boeken_Id, AVG(Reviews.Rating) AS Rating
FROM Reviews
GROUP BY Reviews.Boeken_Id;

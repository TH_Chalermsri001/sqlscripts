-- Oefening 2
-- Schrijf een script, 0540__Oefening.sql, 
-- dat per artiest toont hoeveel liedjes van deze artiest in het systeem zitten
USE ModernWays;
SELECT Artiest, COUNT(*) AS 'Aantal liedjes'
FROM Liedjes
GROUP BY Artiest;

/* Oefening 2:
Pas, niet rechtstreeks in de tabel Boeken maar wel via de view AuteursBoeken, 
de titel Pet Sematary aan naar Pet Cemetery.
Noem het script hiervoor 0614__Oefening.sql:*/

USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE Auteursboeken
SET Titel = "Pet Cemetery" WHERE Titel = "Pet Sematary";
SET SQL_SAFE_UPDATES = 1;
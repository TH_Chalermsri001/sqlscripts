-- Oefening 7
-- Schrijf een script, 0545__Oefening.sql, 
-- dat je het totaal aantal beluisteringen toont voor elke artiest 
-- met minstens 10 karakters in de naam, 
-- maar enkel als dat aantal minstens 100 is.
USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Totaal aantal beluisteringen'
FROM Liedjes
WHERE Length(Artiest) >= 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) >= 100;
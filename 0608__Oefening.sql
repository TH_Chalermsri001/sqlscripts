/* Oefening 6
Toon alle taken, met het lid dat de taak uitvoert. 
Als de taak door niemand wordt uitgevoerd, staat er een NULL. 
Doe dit met een soort JOIN die je nog niet hebt gebruikt, dus geen CROSS JOIN of INNER JOIN. 
Voorbeeldoutput (kan anders zijn voor jouw data): */

USE ModernWays;
SELECT Leden.Voornaam, Taken.Omschrijving
FROM Leden
LEFT JOIN Taken ON Leden.Id = Taken.Leden_Id;
USE ModernWays;
	-- Toon met script 0532__Oefening.sql een overzicht van alle muzieknummers, 
    -- geordend volgens releasejaar (van nieuw naar oud), artiest (van A tot Z) 
    -- en lengte van de titel (van lang naar kort). Dit laatste kan je bereiken met de LENGTH-functie, 
    -- die vergelijkbaar is met CONCAT en SUBSTRING maar slechts één invoer heeft.
    
    SELECT * FROM liedjes ORDER BY Jaar DESC, Artiest ASC, LENGTH(Titel) DESC;
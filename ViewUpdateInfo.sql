/* Oefening 4
Vraag via de metadatabank op welke van je views updatable zijn en welke niet. 
Schrijf voor de meest complexe van de updatable views een UPDATE per kolom om te testen of je die kolom kan updaten. 
Je hoeft het script niet te bewaren, maar je moet in een gelijkaardige situatie (bv. op het examen) 
wel kunnen zeggen welke kolommen updatable zijn: */

USE ModernWays;
SELECT table_name, is_updatable
FROM information_schema.views
WHERE table_schema = 'ModernWays';
-- Oefening 3
-- Schrijf een script, 0541__Oefening.sql, 
-- dat je toont hoe alle nummers samen van één artiest zijn beluisterd. 
-- Dus als bijvoorbeeld "Stairway to Heaven" 10 keer beluisterd is en "Rock and Roll" 7 keer, 
-- dan zijn alle nummers van Led Zeppelin samen 17 keer beluisterd. 
-- Jouw script moet dit voor alle artiesten uitrekenen.

USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Aantal beluisteringen'
FROM Liedjes
GROUP BY Artiest;
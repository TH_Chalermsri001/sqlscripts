/* Oefening 7
Toon alle titels van games met hun bijbehorend platform, als er een is. 
Toon ook games waarvoor het platform niet meer ondersteund wordt 
(d.w.z. waarvoor geen info in Releases staat). 
Gebruik hiervoor een samenstelling van twee JOINs. 
Noem het script 0609__Oefening.sql. */

USE ModernWays;
SELECT Games.Titel, Platformen.Naam
FROM Releases JOIN Platformen ON Platformen.Id = Releases.Platformen_Id
RIGHT JOIN Games ON Games.Id = Releases.Games_Id;
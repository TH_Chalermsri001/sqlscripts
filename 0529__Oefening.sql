USE ModernWays;
	-- Voeg met een script 0529__Oefening.sql volgende twee boeken toe, 
    -- exact zoals gegeven (laat andere kolommen weg):
	INSERT INTO Boeken (Familienaam, Titel, Verschijningsjaar, Categorie)
		VALUES
		('?','Beowulf','0975','Mythologie'),
		('Ovidius','Metamorfosen','8','Mythologie');
		
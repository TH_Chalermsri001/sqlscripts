-- VREEMDE SLEUTELS
-- Oefening 2
-- Vul de juiste waarden in in de kolom die je net hebt toegevoegd. 
-- Je mag dit voorlopig doen zoals je zelf wil, als het juiste baasje maar naar het juiste dier verwijst. 
-- Noem dit 0553__Oefening.sql

USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE Baasjes -- update huisdieren id
SET huisdieren_id = 5 
WHERE Baasje = 'Lyssa';
SET SQL_SAFE_UPDATES = 1;
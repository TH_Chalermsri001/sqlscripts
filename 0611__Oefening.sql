/* Oefening 9 (Een uitdaging!)
Toon alle games waarvoor het platform onbekend is, 
samen met alle platformen waarvoor geen games beschikbaar zijn. 
Noem je script 0611__Oefening.sql: */

USE ModernWays;
SELECT Games.Titel,
	COALESCE ( Releases.Games_Id, 'Geen platformen gekend' ) AS Platformen
FROM Games LEFT JOIN Releases ON Id = Games_Id
WHERE Games_Id IS NULL
	UNION
SELECT COALESCE ( Releases.Platformen_Id, 'Geen games gekend' ), Platformen.Naam
FROM Releases RIGHT JOIN Platformen ON Platformen_Id = Id
WHERE Platformen_Id IS NULL;
/* Oefening 1:
Maak een view aan met naam AuteursBoeken 
waarmee je makkelijk een overzicht kan vragen van welke auteur welk(e) boek(en) heeft geschreven: */

USE ModernWays;
CREATE VIEW Auteursboeken
AS
SELECT CONCAT(Personen.Voornaam,' ', Personen.Familienaam) AS 'Auteur', Boeken.Titel
FROM Publicaties
INNER JOIN Personen ON Personen.Id = Publicaties.Personen_Id
INNER JOIN Boeken ON Boeken.Id = Publicaties.Boeken_Id;
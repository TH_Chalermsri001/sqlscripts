USE ModernWays;
	CREATE TABLE Metingen(
		Tijdstip datetime not null,
        Grootte smallint unsigned not null,
        Marge decimal (3,2)
    );
    
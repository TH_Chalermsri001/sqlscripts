-- Primaire sleutels
-- Oefening 2
-- Een nieuw tabel aanmaken met een Id sleutel
-- Maak een nieuwe tabel, Baasjes, met één "gewone" kolom voor de naam van het baasje. 
-- Deze kolom heeft dezelfde eigenschappen als de bestaande kolom Baasje in de tabel Huisdieren. Om deze te achterhalen: rechterklik op Huisdieren, gebruik "table inspector" en lees het type af, check of NULL toegestaan is en let op de karakterset. Op andere zaken hoef je nu niet te letten. De tabel heeft ook een primaire sleutel die de gewoonlijke conventies voor primaire sleutelkolommen volgt. 
-- Noem het script om de tabel te maken 0550__Oefening.sql.

USE ModernWays;
-- nieuwe kolom toevoegen:
CREATE TABLE Baasjes (
Baasje VARCHAR(100) CHARSET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
Id INT AUTO_INCREMENT PRIMARY KEY
);

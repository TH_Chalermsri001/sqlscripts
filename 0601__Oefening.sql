/* Veel-op-veel-relaties:
Oefening 1:
Merk op dat er tabellen zijn met de namen Boeken en tabel Auteurs. 
Gebruik volgende informatie om de tabel BoekenNaarAuteurs in te vullen:
* Haruki Murakami schreef Norwegian Wood en Kafka on the Shore
* Neil Gaiman schreef American Gods en The Ocean at the End of the Lane
* Stephen King schreef Pet Sematary
* Terry Pratchett en Neil Gaiman schreven samen Good Omens
* Stephen King en Peter Straub schreven samen The Talisman
0601__Oefening.sql*/

USE ModernWays;
INSERT INTO BoekenNaarAuteurs 
(Boeken_Id, Auteurs_Id)
VALUES
(1,10),
(2,10),
(3,16),
(4,16),
(5,17),
(6,16),
(6,18),
(7,17),
(7,19);
USE ModernWays;
	-- Schrijf een script, 0534__Oefening.sql, 
    -- waarmee alle kolommen van alle boeken gezocht worden 
    -- waarvan de familienaam van de auteur begint met een A, R of C.
    
    SELECT * FROM Boeken WHERE Familienaam LIKE 'A%' OR Familienaam LIKE 'R%' OR Familienaam LIKE 'C%';
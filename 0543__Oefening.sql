-- Oefening 5
-- Schrijf een script, 0543__Oefening.sql, 
-- dat je het gemiddeld aantal beluisteringen van nummers van Layla Zoe toont. 
-- Het formaat is (met in dit geval exact één rij):
USE ModernWays;
SELECT Artiest, AVG(Aantalbeluisteringen) AS 'Gemiddeld aantal beluisteringen'
FROM Liedjes
WHERE Artiest = 'Layla Zoe'
GROUP BY Artiest;
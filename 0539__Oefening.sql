-- Groeperen en berekenen
-- Oefening 1
-- Schrijf een script, 0539__Oefening.sql, dat je toont:
-- hoe oud een huisdier gemiddeld is
-- hoe oud het oudste huisdier is
-- hoeveel huisdieren er zijn

USE ModernWays;
SELECT AVG(Leeftijd) AS 'Gemiddelde leeftijd',
MAX(Leeftijd) AS 'Hoogste leeftijd',
COUNT(*) AS 'Totaal aantal'
FROM Huisdieren;
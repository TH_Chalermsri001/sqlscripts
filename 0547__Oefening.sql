-- Oefening 2
-- Schrijf een script, 0547__Oefening.sql, 
-- dat alle artiesten toont waarvan de naam alfabetisch tussen de 'i' en de 'p' ligt. 
-- Zorg ervoor dat elke artiestennaam slechts één keer wordt getoond. 
-- Dit moet hoofdletterongevoelig en accentongevoelig zijn 
-- en los dit op zonder gebruik te maken van GROUP BY.

USE ModernWays;
SELECT DISTINCT Artiest
FROM Liedjes
WHERE Artiest COLLATE utf8mb4_0900_ai_ci BETWEEN 'i' AND 'p';
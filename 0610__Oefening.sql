/* Oefening 8
Toon alle platformen waarvoor games beschikbaar zijn. 
Toon dus geen platformen indien geen games beschikbaar zijn. 
Noem je script 0610__Oefening.sql. */

USE ModernWays;
SELECT DISTINCT Platformen.Naam
FROM Platformen INNER JOIN Releases ON Platformen.Id = Releases.Platformen_Id;